/**
 * @file mainwindow.h
 * @brief Définition de la classe MainWindow.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialogButtonBox>
#include <QGraphicsScene>
#include <QLineEdit>
#include <QMainWindow>
#include <QTextEdit>
#define TAILLE 50
#define NBR_CELL 8


/**
 * @class MainWindow
 * @brief La classe MainWindow est une fenêtre principale pour le jeu Reversi.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief Constructeur de la classe MainWindow.
     * @param parent Le widget parent.
     */
    MainWindow(QWidget *parent = nullptr);

    /**
     * @brief Destructeur de la classe MainWindow.
     */
    ~MainWindow();


private slots:
    /**
     * @brief Slot pour quitter l'application.
     */
    void slotQuitter();

    /**
     * @brief Slot pour jouer un coup.
     */
    void slotJouer();
private:
    QMenuBar *menuBar; /**< Barre de menu. */
    QTextEdit *smallEditor;  /**< Éditeur de texte pour les petits textes. */
    QTextEdit *bigEditor;/**< Éditeur de texte pour les gros textes. */
    int joueur = 1; /**< Le joueur courant (1 ou 2). */

    QMenu *fileMenu; /**< Menu Fichier. */
    QAction *exitAction; /**< Action pour quitter l'application. */
     QDialogButtonBox *buttonBox; /**< Boîte de boutons pour les dialogues. */


    const QString ETIQUETTE = "ABCDEFGH"; /**< Chaîne pour les étiquettes de colonne. */
    int tableau[NBR_CELL][NBR_CELL];  /**< Tableau représentant l'état du jeu. */
    QLineEdit *choixXY;  /**< Zone de texte pour entrer les coordonnées. */
    QGraphicsScene *scene; /**< Scène pour le dessin du jeu. */


    /**
     * @brief Dessine le jeu sur la scène.
     */
    void dessinerJeu();

    /**
     * @brief Initialise le jeu avec les pièces initiales.
     */
    void initialiserJeu();

    /**
     * @brief Dessine une pièce sur la scène.
     * @param aCol La colonne où dessiner la pièce.
     * @param aLigne La ligne où dessiner la pièce.
     * @param aJoueurId L'identifiant du joueur possédant la pièce.
     */
    void dessinerPiece(int aCol, int aLigne, int aJoueurId);

    /**
     * @brief Initialise les pièces du jeu.
     */
    void intialiserPiece();


    /**
     * @brief Capture les pions de l'adversaire dans la direction donnée.
     * @param col La colonne où se trouve la pièce jouée.
     * @param ligne La ligne où se trouve la pièce jouée.
     * @param joueur L'identifiant du joueur qui joue la pièce.
     * @return Vrai si des pions ont été capturés, faux sinon.
     */
    bool capturerPions(int col, int ligne, int joueur);

    /**
     * @brief Vérifie si une pièce adjacente est déjà posée.
     * @param col La colonne où se trouve la pièce à poser.
     * @param ligne La ligne où se trouve la pièce à poser.
     * @return Vrai si une pièce adjacente est déjà posée, faux sinon.
*/
    bool verifierPieceAjacente(int col, int ligne);
};
#endif // MAINWINDOW_H
