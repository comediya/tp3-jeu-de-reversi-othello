
/**
 * @file mainwindow.cpp
 * @brief Définition de la classe MainWindow pour le jeu Reversi.
 */
#include "mainwindow.h"
#include <QApplication>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QMenu>
#include <QMenuBar>
#include <QPushButton>



/**
 * @class MainWindow
 * @brief Classe représentant la fenêtre principale de l'application Reversi.
 *
 * Cette classe gère l'affichage du plateau de jeu, la gestion des pièces, la capture de     celles-ci, ainsi que les boutons et menus associés.
 */

/**
  * @brief Constructeur de la classe MainWindow.
  * @param parent Pointeur vers le widget parent.
  *
  * Initialise la fenêtre principale avec le plateau de jeu, les boutons et les menus associés.
  */

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *fenetre = new QWidget(this);
    QVBoxLayout *grille = new QVBoxLayout();
    fenetre->setLayout(grille);
    scene = new QGraphicsScene;
    initialiserJeu();
    QGraphicsView *view = new QGraphicsView(scene);// la vue du dessin ou son affichage graphique different de la scene qui est la où il va s'appliquer
    grille->addWidget(view);// 1

    choixXY = new QLineEdit;
    choixXY->setFocus();
    choixXY->setInputMask("A9");
    QFormLayout *xyChoisieForm = new QFormLayout;
    xyChoisieForm->addRow("XY", choixXY);
    grille->addLayout(xyChoisieForm);
    choixXY->setCursorPosition(0);

    QPushButton *boutonJouer = new QPushButton("Jouer");
    grille->addWidget(boutonJouer);
    QPushButton *boutonPasserTour = new QPushButton("Passer son tour");
    grille->addWidget(boutonPasserTour);
    QPushButton *quitter = new QPushButton("Quitter");
    grille->addWidget(quitter);

    connect(quitter, &QPushButton::clicked, this, &MainWindow::slotQuitter);
    connect(boutonJouer, &QPushButton::clicked, this, &MainWindow::slotJouer);

    setCentralWidget(fenetre);
}

/**
 * @brief Dessine le plateau de jeu.
 *
 * Il dessine le plateau de jeu en créant des cases de couleur verte pour chaque emplacement. Elle ajoute également les coordonnées du plateau (lettres et chiffres) et les pièces (noires et blanches) déjà présentes.
 * @return void
 */
void MainWindow::dessinerJeu() {
    scene->clear();

    for(int i =0; i<NBR_CELL; i++) {
        // dessine les coordonnées
        QGraphicsTextItem *txt = new QGraphicsTextItem();
        txt->setPlainText(ETIQUETTE[i]);
        txt->setPos((i+1)*(TAILLE)+TAILLE/2, 20);// position où mettre les lettres
        scene->addItem(txt);
        txt = new QGraphicsTextItem();
        txt->setPlainText(QString::number(i+1));
        txt->setPos(20, (i+1.5)*(TAILLE));// position où mettre les chiffres
        scene->addItem(txt);

        // dessine les cases
        for(int j=0; j<NBR_CELL; j++) {
            QGraphicsRectItem *rectItem =  // petit rectangle et prend le X et le Y
                new QGraphicsRectItem(QRectF((i+1)*TAILLE, (j+1)*TAILLE, TAILLE, TAILLE));

            //calcul l'emplacement des cases fonctionne par colonne ou commence par colonne
            rectItem->setBrush(Qt::green);
            scene->addItem(rectItem);

            if(tableau[i][j] != 0)
            {

                qDebug() << i << j << tableau[i][j]; // affiche en  console

                dessinerPiece(i,j,tableau[i][j]); // passe les coordonner
            }
        }
    }
}

/**
*@brief Initialise le jeu en réinitialisant le tableau de jeu.
* @return void
*/
void MainWindow::initialiserJeu()
{
    for(int i=0; i<NBR_CELL; i++) {
        for(int j=0; j<NBR_CELL; j++) {
            tableau[i][j] = 0;
        }
    }
    intialiserPiece();
    dessinerJeu();

}

/**
 * @brief Dessine un jeton de couleur noire ou blanche sur la grille de jeu.
 * @param col La colonne dans laquelle le jeton sera dessiné.
 * @param ligne La ligne dans laquelle le jeton sera dessiné.
 * @param joueurId L'identifiant du joueur (1 pour le joueur noir, 2 pour le joueur blanc).
 * @return void
 */
void MainWindow::dessinerPiece(int col, int ligne, int joueurId) {
    QGraphicsEllipseItem *rond = new QGraphicsEllipseItem((col+1.25)*TAILLE, (ligne+1.25)*TAILLE, 20, 20);

    if(joueurId==1) {
        rond->setBrush(Qt::black);
    } else {
        rond->setBrush(Qt::white);
    }
    scene->addItem(rond);
};

/**
 * @brief Initialise les pièces du jeu en positionnant 4 jetons sur les cases centrales du plateau et en les dessinant.
 *
 * @details Les jetons sont positionnés aux coordonnées (3, 3), (3, 4), (4, 3) et (4, 4) du tableau de jeu. Les jetons sont ensuite dessinés à l'aide de la fonction dessinerPiece().
 *
 * @return void
 */
void MainWindow::intialiserPiece(){

    tableau[3][3] = 1;
    tableau[3][4] = 2;
    tableau[4][3] = 2;
    tableau[4][4] = 1;

    dessinerPiece(3, 3, 1);
    dessinerPiece(3, 4, 2);
    dessinerPiece(4, 3, 2);
    dessinerPiece(4, 4, 1);

};

/**

* @brief Vérifie si une pièce a des pièces adjacentes sur le plateau de jeu.
*
* @param col La colonne de la pièce à vérifier.
*
* @param ligne La ligne de la pièce à vérifier.
*
* @return true Si une pièce adjacente a été trouvée.
*
* @return false Si aucune pièce adjacente n'a été trouvée.
*/
bool MainWindow::verifierPieceAjacente(int col, int ligne){
    bool indicateur = false;

    for(int i=-1; i<2; i++){
        for(int j=-1; j<2; j++){
            // Vérifier si la case est en dehors du tableau
            if (col + i < 0 || col + i >= NBR_CELL  ||
                ligne + j < 0 || ligne + j >= NBR_CELL ){

            }
            // Vérifier si la case contient une pièce
            if (tableau[col + i][ligne + j] != 0){
                indicateur = true;
            }
        }
    }
    return indicateur;
}


/**
 * @brief Vérifie si le joueur peut capturer des pions adverses dans une certaine case.
 *
 * @param col La colonne où se trouve la case à vérifier.
 * @param ligne La ligne où se trouve la case à vérifier.
 * @param joueur L'identifiant du joueur qui veut capturer les pions adverses.
 * @return bool Retourne `true` si le joueur a capturé des pions adverses, `false` sinon.
 */
bool MainWindow::capturerPions(int col, int ligne, int joueur){
    bool capturer = false;


    // On détermine le numéro de l'autre joueur
    int autreJoueur;
    if (joueur == 1) {
        autreJoueur = 2;
    } else {
        autreJoueur = 1;
    }


    for(int i=-1; i<2; i++){
        for(int j=-1; j<2; j++){


            int x = col + i;
            int y = ligne + j;
            int nbPions = 0;
            // Tant qu'on trouve des pions de l'autre joueur
            while((tableau[x][y] == autreJoueur)&& (col + i < 0 || col + i >= NBR_CELL  ||
                                                      ligne + j < 0 || ligne + j >= NBR_CELL )){
                // On compte le nombre de pions dans cette direction
                nbPions++;
                x += i;
                y += j;
            }
            // Si on a trouvé des pions à capturer et que la case suivante est celle du joueur actuel
            if(nbPions > 0 && tableau[x][y] == joueur){
                capturer = true;
                x = col + i;
                y = ligne + j;

                // On parcourt à nouveau la ligne pour capturer les pions
                 for(int k = 0; k < nbPions; k++){
                    tableau[x][y] = joueur;
                    x -= i;
                    y -= j;
                    dessinerPiece(x, y, autreJoueur);
                }
            }
        }
    }

    return capturer;
}


/**
* @brief Destructeur de la classe MainWindow
* Cette fonction est appelée lorsque l'objet MainWindow est détruit. Elle se charge de libérer les ressources
    */
MainWindow::~MainWindow()
{
}



//slots


/**
 * @brief Slot appelé lorsqu'un joueur joue un coup.
 * Vérifie si les coordonnées du coup sont valides et si la case est vide. Si oui,
 * la case est remplie avec le pion du joueur actuel, le plateau est redessiné et
 * le tour passe au joueur suivant. Si non, rien ne se passe.
    */
void MainWindow::slotJouer() {
    if(!(choixXY->text().isEmpty())) {
        QString colTxt = choixXY->text()[0];
        QString ligneTxt = choixXY->text()[1];
        int col = ETIQUETTE.indexOf(colTxt.toUpper());
        int ligne = ligneTxt.toInt()-1;
        //8
        if(tableau[col][ligne]==0
            && col>=0 && col < NBR_CELL
            && ligne >=0 && ligne < NBR_CELL) {
            tableau[col][ligne] = joueur;
            dessinerJeu();
            joueur =  ((joueur)%2) +1;
        } else {
            // 5

        }
    }
    choixXY->clear();
    choixXY->setFocus();
    choixXY->setCursorPosition(0);

}


/**
* @brief Slot appelé lorsque l'utilisateur quitte l'application.
* Ferme l'application.
    */
void MainWindow::slotQuitter()
{
    QApplication::quit();
}

