## Jeu de Reversi


Reversi est un jeu de société classique également connu sous le nom d'Othello. Le jeu est joué par deux joueurs, chacun ayant un jeu de pions de couleur. Le but du jeu est de capturer le plus de pions possible en les retournant pour qu'ils soient de votre couleur.

Ce projet est une implémentation de Reversi en C++ utilisant la bibliothèque graphique Qt. Le jeu est conçu pour être joué en mode joueur contre joueur.


## Installation
    - Téléchargez et installez Qt Creator à partir du site officiel de Qt.
    - Clonez ou téléchargez le projet depuis le dépôt GitHub.
    - Ouvrez le projet dans Qt Creator.
    - Compilez le projet en utilisant le bouton "Build" dans Qt Creator.

## Comment jouer


Une fois le jeu lancé, les joueurs doivent se mettre d'accord sur qui joue en premier. Le joueur 1 (pièces noires) joue en premier. Les joueurs prennent tour à tour leur tour en cliquant sur l'emplacement sur le plateau où ils souhaitent jouer.

Les règles du jeu sont les suivantes :

    - Les joueurs jouent à tour de rôle en plaçant un pion de leur couleur sur une case vide du plateau.
    - Si un pion est posé à côté d'un pion adverse, tous les pions adjacents du joueur adverse dans cette direction sont retournés pour devenir la couleur du joueur qui vient de jouer.
    - Si un joueur ne peut pas poser de pion sur le plateau, il doit passer son tour.
    - Le jeu se termine lorsque le plateau est rempli ou lorsque les deux joueurs ne peuvent plus jouer.
    - Le joueur avec le plus de pions de sa couleur sur le plateau à la fin du jeu remporte la partie.



    Bonne chance et amusez-vous bien !



Source: https://www.ffothello.org/othello/regles-du-jeu/