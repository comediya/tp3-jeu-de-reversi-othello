
/**

* @file main.cpp
* @brief Programme principal pour le jeu reversi
* Programme principal pour le jeu reversi.
* Ce programme crée une instance de la classe MainWindow,
puis la montre et exécute l'application.

*/
#include "mainwindow.h"

#include <QApplication>




/**
* @brief Point d'entrée du programme
* @param argc Le nombre d'arguments du programme
* @param argv Les arguments du programme
* @return Le code de sortie de l'application
* Le point d'entrée du programme crée une instance de la classe MainWindow,
* la montre à l'écran et exécute l'application.
*/
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
